#!/bin/bash

# Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

set -e

zone="$1"
subdomain="$2"
interval_min="$3"
interval_avg="$4"
interval_mem="$5"

now=`date +%s`
charge=0

case $subdomain in
'@')	files=_			;;
*)	files=$subdomain	;;
esac

if test -f $files,timings && read lastup charge <$files,timings
then
	if [ $now -lt $[ $lastup + $interval_min ] ]; then
		echo "wait $[ $lastup + $interval_min - $now ]"
		echo >&2 "must wait at least $interval_min between updates"
		exit 75
	fi
	charge=$[ $charge + $interval_avg - ($now - $lastup) ]
	if [ $charge -gt $interval_mem ]; then
		echo "wait $[ $charge - $interval_mem ]"
		echo >&2 "must wait on average $interval_avg between updates"
		exit 75
	fi
	if [ $charge -lt 0 ]; then charge=0; fi
fi

sort >$files,new

if test -f $files,data
then
	set +e
	diff >/dev/null $files,data $files,new
	diff=$?
	set -e

	if [ $diff = 0 ]; then echo 'unchanged'; exit 0; fi
	if [ $diff != 1 ]; then exit 1; fi
fi

echo $now $charge >$files,timings.new
mv -f $files,timings.new $files,timings
mv $files,new $files,data

exec /usr/share/userv/dyndns/install $zone
