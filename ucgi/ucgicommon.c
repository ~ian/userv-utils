/*
 * Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 * Copyright 2013,2016 Mark Wooding <mdw@distorted.org.uk>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#include <assert.h>
#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#include <unistd.h>

#include "ucgi.h"

int debugmode= 0;

static void outerror(void) {
  perror("stdout");
  exit(debugmode ? 0 : -1);
}

void syserror(const char *m) {
  if (printf("Content-Type: text/plain\n"
	     "Status: 500\n\n"
	     "ucgi: system call error:\n"
	     "%s: %s\n",
	     m,strerror(errno))==EOF || fflush(stdout)) outerror();
  exit(0);
}

void error(const char *m, int st) {
  if (printf("Content-Type: text/plain\n"
	     "Status: %d\n\n"
	     "ucgi: error:\n"
	     "%s\n",
	     st, m)==EOF || fflush(stdout)) outerror();
  exit(0);
}

void *xmalloc(size_t sz) {
  void *r;

  r= malloc(sz);
  if (!r) syserror("malloc failed");
  return r;
}

void *xrealloc(void *ptr, size_t sz) {
  void *r;

  r= realloc(ptr,sz);
  if (!r) syserror("realloc failed");
  return r;
}

void xsetenv(const char *en, const char *ev, int overwrite) {
  if (setenv(en,ev,overwrite)) syserror("setenv");
}

const char **load_filters(unsigned flags, const char *first, ...) {
  va_list ap;
  const char *name, *p, *q, **v;
  char *pp;
  size_t l, n, sz;
  FILE *fp;
  char buf[MAX_ENVVAR_NAME];

  D( if (debugmode) printf(";; load_filters...\n"); )
  va_start(ap, first);
  for (name= first; name; name= va_arg(ap, const char *)) {
    fp= fopen(name, "r"); if (fp) goto opened;
    D( if (debugmode)
	 printf(";;   file `%s': %s\n", name, strerror(errno)); )
    if (errno != ENOENT) syserror("failed to open environment filters");
  }
  va_end(ap);
  if (flags & LOADF_MUST) syserror("failed to open environment filters");
  D( if (debugmode) printf(";;   using default filters\n"); )
  return 0;

opened:
  va_end(ap);
  D( if (debugmode) printf(";;   file `%s': OK\n", name); )

  n= 0; sz= 128; v= xmalloc(sz * sizeof(*v));
  for (;;) {
    if (!fgets(buf, sizeof(buf), fp)) break;
    l= strlen(buf);
    if (buf[l - 1] == '\n') buf[--l]= 0;
    if (l + 1 == sizeof(buf))
      error("line too long in environment filter file", 500);
    p= buf; q= p + l;
    while (isspace((unsigned char)*p)) p++;
    while (q > p && isspace((unsigned char)q[-1])) q--;
    if (*p == '#' || p == q) continue;
    l= q - p;
    pp= xmalloc(l + 1);
    memcpy(pp, p, l);
    pp[l]= 0;
    v[n++]= pp;
    D( if (debugmode) printf(";;   filter: `%s'\n", pp); )
    if (n >= sz) {
      sz *= 2;
      v= xrealloc(v, sz * sizeof(*v));
    }
  }
  if (ferror(fp)) syserror("failed to read environment filters");
  fclose(fp);
  return v;
}

static int envvar_match(unsigned flags, const char *en,
			const char *const *patv,
			const char *const *defaults,
			const char **ev) {
  const char *const *patp;
  const char *q, *pat;
  int acceptp;
  int rc;

  if (!patv) { patv= defaults; defaults= 0; }
  for (patp= patv; (pat= *patp); patp++) {
    q= en;
    acceptp= 1;
    if (*pat == '!' && (flags & FILTF_WILDCARD)) { acceptp= 0; pat++; }
    else if (*pat == '?') {
      if (strcmp(pat + 1, "DEFAULTS") == 0) {
	assert(defaults);
	rc= envvar_match(flags, en, defaults, 0, ev);
	if (rc) return rc;
      } else
	error("unknown pattern directive", 500);
      continue;
    }

    for (;;) {
      if (!*pat) {
	if (*q != '=') {
	  D( if (debugmode)
	       printf(";;     mismatch `%s' (prefix)\n", *patp); )
	  break;
	}
	D( if (debugmode) printf(";;     matched pattern `%s'\n", *patp); )
	goto match;
      } else if (*pat == '*' && (flags & FILTF_WILDCARD)) {
	q = strchr(q, '=');
	if (!q) {
	  D( if (debugmode)
	       printf(";;     mismatch `%s' (discard: no `=')\n", *patp); )
	  return -1;
	}
	D( if (debugmode)
	     printf(";;     wildcard match for `%s'\n", *patp); )
	goto match;
      } else {
	if (*pat++ != *q++) {
	  D( if (debugmode) printf(";;     mismatch `%s'\n", *patp); )
	  break;
	}
      }
    }
  }
  return 0;

match:
  if (!acceptp) return -1;
  *ev= q + 1;
  return +1;
}

void filter_environment(unsigned flags, const char *prefix_in,
			const char *const *patv,
			const char *const *defaults,
			void (*foundone)(const char *fulln,
					 const char *en, const char *ev,
					 void *p),
			void *p) {
  char *const *ep;
  const char *en, *ev;
  char enbuf[MAX_ENVVAR_NAME];
  size_t n, pn = strlen(prefix_in);

  D( if (debugmode) printf(";; filter_environment...\n"); )
  for (ep= environ; (en= *ep); ep++) {
    D( if (debugmode) printf(";;   consider env-var `%s'\n", en); )
    if (strncmp(en, prefix_in, pn) != 0 || !en[pn]) {
      D( if (debugmode) printf(";;     doesn't match prefix\n"); )
      continue;
    }
    if (envvar_match(flags, en + pn, patv, defaults, &ev) > 0) {
      n= strcspn(en, "=");
      if (n >= sizeof(enbuf))
	error("environment variable name too long", 500);
      memcpy(enbuf, en, n);
      enbuf[n]= 0;
      D( if (debugmode)
	   printf(";;     full = `%s'; tail = `%s'; value = `%s'\n",
		  enbuf, enbuf + pn, ev); )
      foundone(enbuf, enbuf + pn, ev, p);
    }
  }
}
