/*
 * Usage: as CGI script
 */
/*
 * Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 * Copyright 2013,2016 Mark Wooding <mdw@distorted.org.uk>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>

#include "ucgi.h"

static const char *const default_envok[] = {
  "AUTH_TYPE",
  "CONTENT_TYPE",
  "CONTENT_LENGTH",
  "DOCUMENT_ROOT",
  "GATEWAY_INTERFACE",
  "HTTP_*",
  "HTTPS",
  "PATH_INFO",
  "PATH_TRANSLATED",
  "QUERY_STRING",
  "REDIRECT_*",
  "REMOTE_*",
  "REQUEST_METHOD",
  "REQUEST_URI",
  "SCRIPT_*",
  "SERVER_*",
  "SSL_*",
  0
};

struct buildargs {
  const char **v;
  int n, max;
};

static void addarg(struct buildargs *args, const char *a) {
  if (args->n > args->max) error("too many arguments", 500);
  args->v[args->n++]= a;
}

static void add_userv_var(const char *fulln,
			  const char *en, const char *ev, void *p) {
  struct buildargs *args= p;
  size_t l;
  char *a;

  l= strlen(ev);
  if (l > MAX_ENVVAR_VALUE) error("environment variable too long", 500);
  a= xmalloc(strlen(en)+l+6);
  sprintf(a,"-DE_%s=%s",en,ev);
  addarg(args, a);
}

int main(int argc, const char **argv) {
  char *username;
  const char *slash2, *pathi, *ev, *av;
  const char *const *envok = 0;
  size_t usernamelen, l;
  struct buildargs args;
  pid_t child, rchild;
  int status;

  l= strlen(argv[0]);
  if (l>6 && !strcmp(argv[0]+l-6,"-debug")) debugmode= 1;

  if (debugmode) {
    if (fputs("Content-Type: text/plain\n\n",stdout)==EOF || fflush(stdout))
      syserror("write stdout");
    if (dup2(1,2)<0) { perror("dup stdout to stderr"); exit(-1); }
    D( printf(";;; UCGI\n"); )
  }
  
  if (argc > MAX_ARGS) error("too many arguments", 500);

  ev= getenv("UCGI_ENV_FILTER");
  if (ev)
    envok= load_filters(LOADF_MUST, ev, LF_END);
  else
    envok= load_filters(0, "/etc/userv/ucgi.env-filter", LF_END);

  pathi= getenv("PATH_INFO");
  if (!pathi) error("PATH_INFO not found", 500);
  D( if (debugmode) {
       printf(";; find user name...\n"
	      ";;   initial PATH_INFO = `%s'\n",
	      pathi);
  } )
  if (pathi[0] != '/' || pathi[1] != '~')
    error("PATH_INFO must start with /~", 400);
  slash2= strchr(pathi+2,'/');
  if (!slash2) error("PATH_INFO must have more than one /", 400);
  usernamelen= slash2-(pathi+2);
  if (usernamelen > MAX_USERNAME_LEN) error("PATH_INFO username too long", 400);
  username= xmalloc(usernamelen+1);
  memcpy(username,pathi+2,usernamelen); username[usernamelen]= 0;
  D( if (debugmode)
       printf(";;   user = `%s'; tail = `%s'\n", username, slash2); )
  if (!isalpha(username[0]))
    error("username 1st character is not alphabetic", 400);
  xsetenv("PATH_INFO",slash2,1);

  args.n= 0; args.max= argc + MAX_ENVVARS + 10;
  args.v= xmalloc(args.max * sizeof(*args.v));
  
  addarg(&args, "userv");
  if (debugmode) addarg(&args, "-DDEBUG=1");

  filter_environment(FILTF_WILDCARD, "", envok, default_envok,
		     add_userv_var, &args);

  addarg(&args, username);
  addarg(&args, "www-cgi");
  while ((av= (*++argv))) addarg(&args, av);
  addarg(&args, 0);

  if (debugmode) {
    D( fflush(stdout); )
    child= fork(); if (child==-1) syserror("fork");
    if (child) {
      rchild= waitpid(child,&status,0);
      if (rchild==-1) syserror("waitpid");
      printf("\nexit status %d %d\n",(status>>8)&0x0ff,status&0x0ff);
      exit(0);
    }
  }
      
  D( if (debugmode) {
       int i;

       printf(";; final command line...\n");
       for (i = 0; args.v[i]; i++)
	 printf(";;   %s\n", args.v[i]);
       fflush(stdout);
  } )

  execvp("userv",(char*const*)args.v);
  syserror("exec userv");
  return -1;
}
