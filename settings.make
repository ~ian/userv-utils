# common makefile settings for userv-utils

# Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

etcdir=		/etc
prefix=		/usr/local
bindir=		$(prefix)/bin
sbindir=	$(prefix)/sbin
vardir=		/var

libdir=		$(prefix)/lib
sharedir=	$(prefix)/share

docdir=		$(sharedir)/doc

libuserv=	$(libdir)/userv
shareuserv=	$(sharedir)/userv
varlog=		$(vardir)/log
varlib=		$(vardir)/lib
varlibuserv=	$(varlib)/userv

etcuserv=	$(etcdir)/userv
services=	$(etcuserv)/services.d

CFLAGS=	-Wall -Wwrite-strings -Wmissing-prototypes -Wstrict-prototypes \
	-Wpointer-arith -D_GNU_SOURCE -Wno-pointer-sign \
	$(OPTIMISE) $(DEBUG) $(SUBDIR_CFLAGS)
LDFLAGS= $(SUBDIR_LDFLAGS) $(DEBUG)

OPTIMISE=	-O2
DEBUG=		-g

depr ?=		disable
# set depr to '' to enable deprecated output
