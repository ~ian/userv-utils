This package contains a number of small utilities and programs for use
with the `userv' security boundary tool, obtained from
<https://www.chiark.greenend.org.uk/ucgi/~ian/git?p=userv-utils.git;a=summary>.

This package, containing the moderately portable sources and Debian
packaging information, and the resulting Debian binary packages, was
put together by Ian Jackson and Sean Whitton.  For both upstream and
Debian packaging questions, please contact
userv-discuss@chiark.greenend.org.uk.


userv and userv-utils are all free software; you can redistribute them
and/or modify them under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 3 of the
License, or (at your option) any later version.

These programs are distributed in the hope that they will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
General Public License for more details.

You should have received a copy of the GNU General Public License with
your Debian GNU/Linux system, in /usr/share/common-licenses/GPL.
If not, see <http://www.gnu.org/licenses/>.


The utilities and programs under the git-daemon/ subdirectory are
dedicated to the public domain by means of the CC0 1.0 public domain
dedication; see /usr/share/common-licenses/CC0-1.0 on Debian systems.


userv-utils are
Copyright 1996-2013,2016 Ian Jackson <ijackson@chiark.greenend.org.uk>
Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
Copyright 1999,2003
   Chancellor Masters and Scholars of the University of Cambridge
Copyright 2010 Tony Finch <fanf@dotat.at>
Copyright 2013,2016 Mark Wooding <mdw@distorted.org.uk>
