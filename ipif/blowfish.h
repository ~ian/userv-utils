/*
 * This file is part of ipif, part of userv-utils
 *
 * Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#ifndef BLOWFISH__H_INCLUDED
#define BLOWFISH__H_INCLUDED

#include <stdint.h>

#define BLOWFISH_BLOCKBYTES 8
#define BLOWFISH_MAXKEYBYTES 56
#define BLOWFISH__N 16
#define BLOWFISH__PSIZE BLOWFISH__N+2

typedef uint32_t blowfish__p[BLOWFISH__PSIZE];
typedef uint32_t blowfish__s[4][256];

struct blowfish_expandedkey {
  blowfish__p p;
  blowfish__s s;
};

/* It's ok to pass the [_cbc]_(en|de)crypt functions the same
 * input and output pointers.
 */

void blowfish_loadkey(struct blowfish_expandedkey *ek,
		      const uint8_t *key, int keybytes);
void blowfish_encrypt(const struct blowfish_expandedkey *ek,
		      const uint8_t plain[BLOWFISH_BLOCKBYTES],
		      uint8_t cipher[BLOWFISH_BLOCKBYTES]);
void blowfish_decrypt(const struct blowfish_expandedkey *ek,
		      const uint8_t cipher[BLOWFISH_BLOCKBYTES],
		      uint8_t plain[BLOWFISH_BLOCKBYTES]);

struct blowfish_cbc_state {
  struct blowfish_expandedkey ek;
  uint32_t chainl, chainr;
};

void blowfish_cbc_setiv(struct blowfish_cbc_state *cs,
			const uint8_t iv[BLOWFISH_BLOCKBYTES]);
void blowfish_cbc_encrypt(struct blowfish_cbc_state *cs,
			  const uint8_t plain[BLOWFISH_BLOCKBYTES],
			  uint8_t cipher[BLOWFISH_BLOCKBYTES]);
void blowfish_cbc_decrypt(struct blowfish_cbc_state *cs,
			  const uint8_t cipher[BLOWFISH_BLOCKBYTES],
			  uint8_t plain[BLOWFISH_BLOCKBYTES]);

#endif
