/*
 * This file is part of ipif, part of userv-utils
 *
 * Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <sysexits.h>

#include "hex.h"

const char *tohex(uint8_t *data, int len, char *buf) {
  char *p;
  
  for (p= buf;
       len>0;
       len--, data++, p+=2)
    sprintf(p,"%02x",*data);
  return buf;
}

void unhex(const char *what, const char *txt, uint8_t *datar, int *lenr,
	   int minlen, int maxlen) {
  int l, v;
  char buf[3], *ep;

  l= strlen(txt);
  if (l%1) { fprintf(stderr,"odd number of hex digits in %s\n",what); exit(EX_DATAERR); }
  l>>=1;
  if (l<minlen) { fprintf(stderr,"too few hex digits in %s\n",what); exit(EX_DATAERR); }
  if (l>maxlen) { fprintf(stderr,"too many hex digits in %s\n",what); exit(EX_DATAERR); }

  if (lenr) *lenr= l;
  while (l) {
    buf[0]= *txt++;
    buf[1]= *txt++;
    buf[2]= 0;
    v= strtoul(buf,&ep,16);
    if (*ep) { fprintf(stderr,"not hex digit in %s: %c\n",what,*ep); exit(EX_DATAERR); }
    *datar++= v;
    l--;
  }
}
