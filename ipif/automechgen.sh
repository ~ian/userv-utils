#!/bin/sh
# generates automech.h and automech.c

# This file is part of ipif, part of userv-utils
#
# Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

exec >automech.c.new
exec 3>automech.h.new

cat <<END
#include "forwarder.h"
const struct mechanism *const mechanismlists[]= {
END

cat >&3 <<END
#ifndef AUTOMECH_H
#define AUTOMECH_H
END

for m in "$@"; do
	echo "  mechlist_$m,"
	echo "extern const struct mechanism mechlist_$m[];" >&3
done

cat <<END
  0
};
END

cat >&3 <<END
#endif
END

for e in c h; do mv -f automech.$e.new automech.$e; done
