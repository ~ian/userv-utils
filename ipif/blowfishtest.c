/*
 * test program for blowfish; very hard to use (sorry!)
 */
/*
 * This file is part of ipif, part of userv-utils
 *
 * Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
 * Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
 * Copyright 1999,2003
 *    Chancellor Masters and Scholars of the University of Cambridge
 * Copyright 2010 Tony Finch <fanf@dotat.at>
 *
 * This is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with userv-utils; if not, see http://www.gnu.org/licenses/.
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#include "blowfish.h"
#include "hex.h"

int main(void) {
  char buf[200], keybuf[200], plainbuf[200], cipherbuf[200], comparebuf[200], ivbuf[200];
  char keytxt[sizeof(buf)+1], plaintxt[sizeof(buf)+1], ciphertxt[sizeof(buf)+1];
  uint8_t key[BLOWFISH_MAXKEYBYTES*2], plain[100], cipher[100], compare[100];
  uint8_t iv[BLOWFISH_BLOCKBYTES];
  int keysz, plainsz, ciphersz, cskey, csiv, csplain, i;
  struct blowfish_expandedkey ek;
  struct blowfish_cbc_state cs;

  setvbuf(stdout,0,_IOLBF,BUFSIZ);
  buf[sizeof(buf)-2]=0;
  keytxt[sizeof(buf)]= 0;
  plaintxt[sizeof(buf)]= 0;
  ciphertxt[sizeof(buf)]= 0;
  cskey= csiv= csplain= 0;
  while (fgets(buf,sizeof(buf),stdin)) {
    if (buf[sizeof(buf)-2]) { fprintf(stderr,"line too long %s...\n",buf); exit(1); }
    if (sscanf(buf,"ecb %s %s %s\n",keytxt,plaintxt,ciphertxt) ==3) {
      unhex("ecb key",keytxt,key,&keysz,1,sizeof(key));
      unhex("ecb plain",plaintxt,plain,0,BLOWFISH_BLOCKBYTES,BLOWFISH_BLOCKBYTES);
      unhex("ecb cipher",ciphertxt,cipher,0,BLOWFISH_BLOCKBYTES,BLOWFISH_BLOCKBYTES);
      printf("ecb %s %s %s\n",
	     tohex(key,keysz,keybuf),
	     tohex(plain,BLOWFISH_BLOCKBYTES,plainbuf),
	     tohex(cipher,BLOWFISH_BLOCKBYTES,cipherbuf));
      blowfish_loadkey(&ek,key,keysz);
      blowfish_encrypt(&ek,plain,compare);
      if (memcmp(cipher,compare,BLOWFISH_BLOCKBYTES)) {
	fprintf(stderr,"encryption mismatch - got %s\n",
		tohex(compare,BLOWFISH_BLOCKBYTES,comparebuf));
	exit(1);
      }
      blowfish_decrypt(&ek,cipher,compare);
      if (memcmp(plain,compare,BLOWFISH_BLOCKBYTES)) {
	fprintf(stderr,"decryption mismatch - got %s\n",
		tohex(compare,BLOWFISH_BLOCKBYTES,comparebuf));
	exit(1);
      } 
   } else if (sscanf(buf,"key %s\n",keytxt)) {
      unhex("key",keytxt,key,&keysz,1,sizeof(key));
      blowfish_loadkey(&cs.ek,key,keysz);
      cskey= 1;
    } else if (sscanf(buf,"iv %s\n",keytxt)) {
      unhex("iv",keytxt,iv,0,BLOWFISH_BLOCKBYTES,BLOWFISH_BLOCKBYTES);
      csiv= 1;
    } else if (sscanf(buf,"plain %s\n",plaintxt)) {
      unhex("plain",plaintxt,plain,&plainsz,0,sizeof(plain));
      csplain= 1;
    } else if (sscanf(buf,"cbc %s\n",ciphertxt)) {
      if (!cskey || !csiv || !csplain) {
	fprintf(stderr,"failed to specify%s%s%s\n",
		cskey ? "" : " key",
		csiv ? "" : " iv",
		csplain ? "" : " plain");
	exit(1);
      }
      unhex("cbc cipher",ciphertxt,cipher,&ciphersz,0,sizeof(cipher));
      printf("key %s\niv %s\nplain %s\ncipher %s\n",
	     tohex(key,keysz,keybuf),
	     tohex(iv,BLOWFISH_BLOCKBYTES,ivbuf),
	     tohex(plain,plainsz,plainbuf),
	     tohex(cipher,ciphersz,cipherbuf));
      if (plainsz % BLOWFISH_BLOCKBYTES ||
	  ciphersz % BLOWFISH_BLOCKBYTES ||
	  plainsz != ciphersz) {
	fprintf(stderr,"size mismatch plain=%d cipher=%d block=%d\n",
		plainsz,ciphersz,BLOWFISH_BLOCKBYTES);
	exit(1);
      }
      blowfish_cbc_setiv(&cs,iv);
      for (i=0; i<plainsz; i+=BLOWFISH_BLOCKBYTES)
	blowfish_cbc_decrypt(&cs,cipher+i,compare+i);
      if (memcmp(plain,compare,BLOWFISH_BLOCKBYTES)) {
	fprintf(stderr,"decryption mismatch - got %s\n",
		tohex(compare,plainsz,comparebuf));
	exit(1);
      }
      blowfish_cbc_setiv(&cs,iv);
      for (i=0; i<plainsz; i+=BLOWFISH_BLOCKBYTES)
	blowfish_cbc_encrypt(&cs,plain+i,compare+i);
      if (memcmp(cipher,compare,BLOWFISH_BLOCKBYTES)) {
	fprintf(stderr,"encryption mismatch - got %s\n",
		tohex(compare,plainsz,comparebuf));
	exit(1);
      }
    } else if (buf[0]=='#' || buf[0]=='\n') {
    } else {
      fprintf(stderr,"huh ? %s",buf);
    }
  }
  if (ferror(stdin)) { perror("stdin"); exit(1); }
  return 0;
}
