m4_dnl udptunnel-vpn-defaults: default settings for udptunnel-reconf

m4_dnl This file is part of ipif, part of userv-utils
m4_dnl
m4_dnl Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
m4_dnl Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
m4_dnl Copyright 1999,2003
m4_dnl    Chancellor Masters and Scholars of the University of Cambridge
m4_dnl Copyright 2010 Tony Finch <fanf@dotat.at>
m4_dnl
m4_dnl This is free software; you can redistribute it and/or modify it
m4_dnl under the terms of the GNU General Public License as published by
m4_dnl the Free Software Foundation; either version 3 of the License, or
m4_dnl (at your option) any later version.
m4_dnl
m4_dnl This program is distributed in the hope that it will be useful, but
m4_dnl WITHOUT ANY WARRANTY; without even the implied warranty of
m4_dnl MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
m4_dnl General Public License for more details.
m4_dnl
m4_dnl You should have received a copy of the GNU General Public License
m4_dnl along with userv-utils; if not, see http://www.gnu.org/licenses/.

SET(	lcommand,	[udptunnel])
SET(	clock_maxskew,	45)
SET( crypto,
	[-e nonce -e timestamp/V_clock_maxskew/V_clock_maxskew -e pkcs5/8 \
	-e blowfish-cbcmac/128 -e blowfish-cbc/128])

SET(	lpublic,	[`hostname`])
SET(	lport,		[Any])
m4_dnl	rpublic		-- usually set in sites
SET(	rport,		[Command])
m4_dnl	lend		-- usually set in global
m4_dnl	rend		-- usually set in sites
SET(	mtu,		[1000])
SET(	proto,		[slip])
SET(	to_restart,	[10])
SET(	to_poll,	[30])
SET(	to_quit,	[130])
SET(	to_report,	[1800])
SET(	timeouts,	[V_to_poll,V_to_quit,V_to_report])
SET(	lnets,		[-])m4_dnl often overridden in global
SET(	rnets,		[-])m4_dnl often overridden in sites
SET(	forbid_remote,	[V_lnets])

SET(	rcommandprefix,	[])
SET(	rcommand,	[V_rcommandprefix udptunnel])

SET( sshprotoka,	[-o 'ProtocolKeepAlives 300'])
SET( sshstdopts, [-o 'ForwardAgent no' -o 'ForwardX11 no' -o 'BatchMode yes'])
SET( sshverbose,	[-v])
SET( sshopts,		[])
SET( ssh,
	[ssh	V_sshstdopts \
		V_sshprotoka V_sshverbose \
		V_sshopts])
SET( sshinvoke,		[V_ssh V_ruser@V_sshdest])
		
SET(	sshdest,	[V_rpublic])

SET(	sites,		[sites])
SET(	tunnels,	[tunnels])

m4_dnl	varlibvpn	-- global can override

SET(	ipifnetsfile,	[V_varlibvpn/ipif-networks])
SET(	activesfile,	[V_varlibvpn/active-sites])
SET(	activesxinfo,	[])
SET(	passivesfile,	[V_varlibvpn/passive-sites])
SET(	passivesxinfo,	[])

SET(	postconfigure,	[])

SET(	invoke_file,	[V_varlibvpn/command.V_site])
SET(	invoke_head,	[#!/bin/sh])
SET(	invoke_hook,	[])

SET(	syslog_facility,	local2)
SET(	syslog_priority,	info)

SET(	inittab_runlevels,	2345)
SET(	inittab_pfx,		[])
SET(	inittab_sfx,
 [</dev/null 2>&1 | logger -p V_syslog_facility.V_syslog_priority -t tunnel-V_site])
SET(	inittab_command,[V_inittab_pfx V_invoke_file V_inittab_sfx])
SET(	inittab_line, 	[V_inittab_runlevels:respawn:V_inittab_command])

SET( invoke_body,
[set -e
V_invoke_hook
echo "STARTING TUNNEL `date`" >&2
set +e
V_command
rc=$?
set -e
echo "TUNNEL CLOSED rc=$rc" >&2
sleep V_to_restart
echo "TUNNEL MAYRESTART" >&2
exit $rc
])

m4_dnl	lgroup		-- usually set in global

SET(	command,
[V_lcommand \
	V_crypto \
	V_lpublic,V_lport \
	V_rpublic,V_rport \
	V_lend,V_rend,V_mtu,V_proto \
	V_timeouts \
	V_rnets \
	V_lnets \
	V_sshinvoke V_rcommand
	])
