# Makefile for ipif/udptunnel stuff

# This file is part of ipif, part of userv-util
#
# Copyright 1996-2013 Ian Jackson <ijackson@chiark.greenend.org.uk>
# Copyright 1998 David Damerell <damerell@chiark.greenend.org.uk>
# Copyright 1999,2003
#    Chancellor Masters and Scholars of the University of Cambridge
# Copyright 2010 Tony Finch <fanf@dotat.at>
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with userv-utils; if not, see http://www.gnu.org/licenses/.

include ../settings.make

varlibvpn=	$(varlibuserv)/vpn
etcuserv=	$(etcdir)/userv
etcvpn=		$(etcuserv)/vpn

PROGRAM_TARGETS+=
PROGRAM_TARGETS$(depr)+= udptunnel-forwarder udptunnel-reconf

TARGETS+=		service
TARGETS$(depr)+=	blowfishtest
TARGETS+=		$(PROGRAM_TARGETS)

PROGRAMS$(depr)+=	udptunnel
PROGRAMS+=		$(PROGRAM_TARGETS)

DIRS+=			$(libuserv) $(etcuserv) $(services)
DIRS+=			$(docdir)/userv-ipif
DIRS+=			$(etcuserv)/ipif-access
DIRS$(depr)+=		$(bindir) $(varlibvpn) $(shareuserv)

SHAREFILES$(depr)+=	udptunnel-vpn-config.m4 udptunnel-vpn-defaults

MECHFILES=	null pkcs5 timestamp sequence blowfish
MECHOBJS=	$(foreach m, $(MECHFILES), mech-$m.o)

OBJS_FORWARD=	forwarder.o $(MECHOBJS) blowfish.o automech.c utils.c
OBJS_BFTEST=	blowfishtest.o blowfish.o hex.o

all:		$(TARGETS)

install:	all
		mkdir -p $(DIRS)
		cp -b service $(libuserv)/ipif
		cp -b service-wrap $(libuserv)/ipif-access
		set -e; for f in $(PROGRAMS); do cp -b $$f $(bindir)/.; done
		cp ipif $(services)/ipif:new
		set -e; cd $(services); test -f ipif || mv ipif:new ipif
		set -e; for f in $(SHAREFILES); do cp $$f $(shareuserv)/.; done

install-docs:
		sed -n '1,/^$$/p' service.c >$(docdir)/userv-ipif/service.c.txt

install-examples:
		set -e; if [ "x$depr" = x ]; then \
			mkdir -p $(etcvpn); \
			cp *.example $(etcvpn)/.; \
		fi

udptunnel-reconf:	udptunnel-reconf.pl Makefile
		perl -p 						  \
	-e '	print "							' \
	-e '\$$shareuserv= \"$(shareuserv)\";\n				' \
	-e '\$$etcvpn= \"$(etcvpn)\";\n					' \
	-e '\$$varlibvpn= \"$(varlibvpn)\";\n" if m#^\# \@\@\@\-#;	' \
	-e '	$$_="" if m/^\# \@\@\@\-/ .. m/^\# \-\@\@\@/;		' \
			<$< >$@.new
		chmod +x $@.new
		mv -f $@.new $@


udptunnel-forwarder:	$(OBJS_FORWARD)
		$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJS_FORWARD)

blowfishtest:		$(OBJS_BFTEST)
		$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $(OBJS_BFTEST)

automech.h:		automechgen.sh Makefile
		./$< $(MECHFILES)

automech.c:		automech.h

clean:
		rm -f *.o core automech.[ch] *~ ./#*#

realclean distclean:	clean
		rm -f $(TARGETS)

forwarder.o $(MECHOBJS) automech.o utils.o:	forwarder.h automech.h
blowfish.o mech-blowfish.o blowfishtest.o:	blowfish.h
blowfishtest.o hex.o:				hex.h
